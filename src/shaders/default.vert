precision lowp float;



attribute vec3 aPosition;
attribute vec2 aTexCoord;

varying vec2 vTexCoord;


void main() {
    vTexCoord = aTexCoord;

    vec3 pos = aPosition;
    gl_Position = vec4(pos, 1.0);
}