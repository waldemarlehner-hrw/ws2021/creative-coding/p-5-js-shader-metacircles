precision lowp float;

varying vec2 vTexCoord;
uniform float uTarget;
uniform float uScale;
uniform vec2 uOffset;
uniform float uAspect;

const int MAX_AGENT_COUNT = 20;
const int BUFFER_SIZE = MAX_AGENT_COUNT * 7;
uniform int uSize;
uniform float uBuffer[BUFFER_SIZE];


float evaluateCircle(vec2 pixelMappedToFunctionCoordinates, vec2 circlePosition, float size) {
    float xComponent = pixelMappedToFunctionCoordinates.x - circlePosition.x;
    xComponent *= xComponent;
    float yComponent = pixelMappedToFunctionCoordinates.y - circlePosition.y;
    yComponent *= yComponent;
    return size / (xComponent + yComponent);
}



void main() {
    vec2 coord = vTexCoord;

    vec2 pixelValueOnFunction = vec2(coord.x * uAspect * uScale + uOffset.x, coord.y * uScale + uOffset.y );

    float fnEvalSum = 0.0;
    float colorSumValue = 0.0;
    vec4 colorSum = vec4(0.0);

    for(int i = 0; i < MAX_AGENT_COUNT; i++) {
        if(i >= uSize) {
            break;
        }
        vec2 pos = vec2(uBuffer[i*7+0], uBuffer[i*7+1]);
        float size = uBuffer[i*7+2];
        vec4 color = vec4(uBuffer[i*7+3], uBuffer[i*7+4], uBuffer[i*7+5], uBuffer[i*7+6]);

        float fnEval = evaluateCircle(pixelValueOnFunction, pos, size);
        colorSum += color * fnEval;
        fnEvalSum += fnEval;
    }

    colorSum /= fnEvalSum;


    if(fnEvalSum > uTarget) {
        gl_FragColor = colorSum;
    }
    else {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
}
