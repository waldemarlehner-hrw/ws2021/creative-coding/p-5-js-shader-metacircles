import { InputBindingApi, TabPageApi } from "tweakpane";
import IGravityCalcStrategy from "./gravityCalc/IGravityCalcStrategy";
import SceneState from "./SceneState";

type Position = [number, number];
type Velocity = Position;
type Color = {r: number, g: number, b: number, a: number};

class MetaCircle {
    private positionSettingsField!: InputBindingApi<unknown, any>

    public resetMomentum() {
        this.direction = [0, 0];
    }
    public appendToSettings(subTab: TabPageApi, parent: SceneState) {
        subTab.addInput(this as MetaCircle, "color");
        subTab.addInput(this as MetaCircle, "magnitude", {min: 0.1})
        this.positionSettingsField = subTab.addInput(this as any, "_position", )
        const btn = subTab.addButton({title: "Remove"});
        btn.on("click", () => {
            parent.notifyAboutMetaCircleRemoved(this);
        });
    }

    private _position: {x: number, y: number}
    public get position(): Position {
        return [this._position.x, this._position.y];
    }
    private direction: Velocity;

    constructor(public magnitude: number, initialPosition: Position, public color: Color) {
        this._position = {x: initialPosition[0], y: initialPosition[0]};
        this.direction = [0, 0];
    }

    public updatePosition(circles: [Position, number][], dTime: number, gravityCalcStrategy: IGravityCalcStrategy) {
        const force = [0,0]
        for(const circle of circles) {
            const forceFromCircle = gravityCalcStrategy.calculateForce(this.position, circle[0], this.magnitude, circle[1]);
            force[0] += forceFromCircle[0];
            force[1] += forceFromCircle[1];
        }
        
        this.direction[0] += force[0] * dTime;
        this.direction[1] += force[1] * dTime;

        this._position.x += this.direction[0] * dTime;
        this._position.y += this.direction[1] * dTime;

        this.positionSettingsField.refresh();
    }
}

export default MetaCircle;
export {Position, Velocity, Color, MetaCircle};