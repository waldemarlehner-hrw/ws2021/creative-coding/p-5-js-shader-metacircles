import defaultFrag from "./shaders/default.frag?raw"
import defaultVert from "./shaders/default.vert?raw"

abstract class AssetManager {

    public static get defaultFrag() {
        return defaultFrag;
    }

    public static get defaultVert() {
        return defaultVert;
    }

}

export default AssetManager;