import p5 from "p5";
import { Pane } from "tweakpane";
import AssetManager from "./AssetManager";
import AntiProportionalGravityCalcStrategy from "./gravityCalc/AntiProportionalGravityCalcStrategy";
import SceneState from "./SceneState";
import Settings from "./Settings";
import StateLinearizationHelper from "./StateLinearizationHelper";

class MetaCirclesSketch {
	
	private settings: Settings
	private canvas?: p5.Renderer;
	private shader!: p5.Shader;
	private tweakPane: Pane;
	private fpsGraph: any;
	private domElement: HTMLElement

	private sceneState: SceneState

	constructor(private p: p5){
		this.settings = new Settings();
		const domElement = document.getElementById("app");
		if(!domElement) {
			throw new Error("Failed to reference Dom Element")
		}
		this.domElement = domElement;
		let [tp, fpsGraph] = Settings.attachAsTweakPane(this.settings);
		this.tweakPane = tp as any;
		this.sceneState = new SceneState(new AntiProportionalGravityCalcStrategy(), this.tweakPane);
		this.fpsGraph = fpsGraph;
		p.setup = () => this.setup();
		p.draw = () => this.draw();
		p.windowResized = () => this.handleDomElementResize();
	}

	private handleDomElementResize() {
		this.p.resizeCanvas(this.domElement.clientWidth, this.domElement.clientHeight);
		this.shader.setUniform("uAspect", this.domElement.clientWidth / this.domElement.clientHeight);

	}

	private setup() {
		this.canvas = this.p.createCanvas(this.domElement.clientWidth, this.domElement.clientHeight, "webgl");
		this.canvas.parent("app");
		this.p.noStroke();
		this.shader = this.p.createShader(AssetManager.defaultVert, AssetManager.defaultFrag);
		this.p.shader(this.shader);
		this.shader.setUniform("uAspect", this.domElement.clientWidth / this.domElement.clientHeight);
	}

	private draw() {
		this.fpsGraph.begin();
		/// Pass params
		this.shader.setUniform("uTarget", this.settings.target)
		this.shader.setUniform("uScale", this.settings.functionScale)
		this.shader.setUniform("uOffset", [this.settings.xPosOffset, this.settings.yPosOffset])

		let dTime = this.p.deltaTime ?? 0;
		if(Number.isNaN(dTime)) {
			dTime = 0;
		}
		const state = this.sceneState.act(dTime);
		this.shader.setUniform("uSize", state.length);
		const stateLinearized = StateLinearizationHelper.convert(state);
		this.shader.setUniform("uBuffer", stateLinearized);

		/// Draw Geometry
		this.p.quad(-1, -1, 1, -1, 1, 1, -1, 1)
		this.fpsGraph.end();
	}
	
	public static toP5Sketch(): (p5: p5) => void {
		return (p5: p5) => new MetaCirclesSketch(p5);
	}
}

export default MetaCirclesSketch;
