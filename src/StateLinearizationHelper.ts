import { SceneStateObject } from "./SceneState";

abstract class StateLinearizationHelper {
    public static convert(state: SceneStateObject) {
        let returnArray = [];
        for( const entry of state) {
            // each entry is 2+1+4 = 7 floats long
            const color = entry[2];
            const colorArray = [color.r / 255, color.g / 255, color.b / 255, color.a]

            const entryAsArray = [...entry[0], entry[1], ...colorArray];
            returnArray.push(...entryAsArray);
        }
        return returnArray;
    }
}

export default StateLinearizationHelper;