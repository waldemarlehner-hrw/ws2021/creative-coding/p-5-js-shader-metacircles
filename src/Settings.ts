import { Pane } from "tweakpane";
import * as EssentialsPlugin from "@tweakpane/plugin-essentials";

class Settings {

    target: number = 0.5;

    xPosOffset: number = 0;
    yPosOffset: number = 0;
    functionScale: number = 30;

    public static attachAsTweakPane(settings: Settings) {
        const pane = new Pane();
        pane.registerPlugin(EssentialsPlugin)
        const tab = pane.addTab({
            pages: [
                {title: "Parameters"},
                {title: "Agents"}
            ]
        });
        const tab1 = tab.pages[0];
        const fpsGraph = tab1.addBlade({view: "fpsgraph", "index": 0})
        const fnFolder = tab1.addFolder({"title": "Function"});
        fnFolder.addInput(settings, "target", {"min": -1, max: 1});
        fnFolder.addInput(settings, "xPosOffset");
        fnFolder.addInput(settings, "yPosOffset");
        fnFolder.addInput(settings, "functionScale", {"step": 1});
        return [pane, fpsGraph];
    }
}

export default Settings;