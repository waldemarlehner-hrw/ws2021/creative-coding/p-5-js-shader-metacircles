import { Pane, TabApi } from "tweakpane";
import IGravityCalcStrategy from "./gravityCalc/IGravityCalcStrategy";
import MetaCircle, { Color, Position } from "./MetaCircle";

interface NewMetaCircleSettings{ 
    magnitude: number
    color: Color
}

type SceneStateObject = [Position, number, Color][]



class SceneState {


	public act(dTimeInMillis: number) : SceneStateObject {
        const dTime = dTimeInMillis / 1000;
        const stateBeforeCalculation: [Position, number][] = this.metacircles.map ( e => ([e.position, e.magnitude]))
    
        for(const circle of this.metacircles) {
            circle.updatePosition(stateBeforeCalculation, dTime, this.getGravityCalculator());
        }
        return this.metacircles.map( e => ([e.position, e.magnitude, e.color]))
    }
    private metacircles: MetaCircle[] = [];
    
    private newMetaCircleSettings: NewMetaCircleSettings = {
        magnitude: 1,
        color: {r: 100, g: 100, b: 100, a: 1}
    }
    private tab!: TabApi;

    constructor(private gravityCalcStrategy : IGravityCalcStrategy, private pane: Pane) {
        this.rebuildUi(false);
    }

    public getGravityCalculator(): IGravityCalcStrategy {
        return this.gravityCalcStrategy;
    }
 

    public notifyAboutMetaCircleAdded(metacircle: MetaCircle) {
        console.info(this.metacircles)
        if(this.metacircles.length > 20) {
            console.error("no more than 20 Entries are allowed. Ignoring.")
            return;
        }
        this.metacircles.push(metacircle);
        this.rebuildUi();
    }

    public notifyAboutMetaCircleRemoved(metacircle: MetaCircle) {
        const targetIndex = this.metacircles.indexOf(metacircle);
        if(targetIndex > -1) {
            this.metacircles.splice(targetIndex, 1)
            this.rebuildUi();
        }
    }
    private rebuildUi(reset = true) {
        if(reset) {
            this.tab.removePage(1);
            this.tab.addPage({index: 1, title: "Agents"})
        }
        this.tab = this.pane.children[0] as TabApi;
        const subTab = this.tab.pages[1];

        for(const circle of this.metacircles) {
            subTab.addSeparator()
            circle.appendToSettings(subTab, this);
        }
        subTab.addSeparator()
        subTab.addInput(this.newMetaCircleSettings, "color", {label: "New Agent Color"})
        subTab.addInput(this.newMetaCircleSettings, "magnitude", {label: "New Agent Magnitude", min: 0.1})
        const btn = subTab.addButton({title: "Create new"})
        btn.on("click", () => {
            const pos: Position = [0,0]
            const metacircle = new MetaCircle(this.newMetaCircleSettings.magnitude, pos, {...this.newMetaCircleSettings.color});
            this.notifyAboutMetaCircleAdded(metacircle);
        })
        const btn2 = subTab.addButton({title: "Reset Momentum"});
        btn2.on("click", () => this.resetMomentum());

        subTab.selected = true;
    }

    private resetMomentum() {
        for( const entry of this.metacircles) {
            entry.resetMomentum();
        }
    }

}

export default SceneState;
export {SceneStateObject};