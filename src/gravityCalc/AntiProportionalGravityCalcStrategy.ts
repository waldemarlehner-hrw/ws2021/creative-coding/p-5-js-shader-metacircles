import { Position, Velocity } from "../MetaCircle";
import IGravityCalcStrategy from "./IGravityCalcStrategy";

class AntiProportionalGravityCalcStrategy implements IGravityCalcStrategy {

    private static get multiplier() {
        return 5;
    }

    // https://www.desmos.com/calculator/znsk9suyoq?lang=de
    public calculateForce(ownPos: Position, theirPos: Position, ownMagnitude: number, theirMagnitude: number): Position{
        if(ownPos[0] == theirPos[0] && ownPos[1] == theirPos[1]) {
            return [0,0];
        }
        const x = (theirPos[0] - ownPos[0]);
        const y = (theirPos[1] - ownPos[1]);
        const force = AntiProportionalGravityCalcStrategy.calculateForceCoefficient(AntiProportionalGravityCalcStrategy.distance([x,y]));
        const forceWithMagnitudes = force * (theirMagnitude / ownMagnitude)

        const forceDirection : Velocity = [x, y];
        const forceDirectionLength = AntiProportionalGravityCalcStrategy.distance(forceDirection);
        const forceDirectionNormalized = [forceDirection[0] / forceDirectionLength, forceDirection[1] / forceDirectionLength];
        if(forceDirection.some( e => Number.isNaN(e))) {
            //throw new Error()
        }
        return [forceDirectionNormalized[0] * forceWithMagnitudes, forceDirectionNormalized[1] * forceWithMagnitudes];

    }
    private static calculateForceCoefficient(distance: number) {
        if(distance < 1) {
            return this.multiplier;
        }

        return this.multiplier / distance;
    }

    private static distance([x,y]: Position) {
        
        const distanceSquared = x*x + y*y;
        return Math.sqrt(distanceSquared);
    }

}

export default AntiProportionalGravityCalcStrategy;