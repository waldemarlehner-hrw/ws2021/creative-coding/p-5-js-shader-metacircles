import { Position, Velocity } from "../MetaCircle";

interface IGravityCalcStrategy {
    /**
     * 
     * @returns the Change in Velocity of the own Meta Circle.
     */
    calculateForce(ownPos: Position, theirPos: Position, ownMagnitude: number, theirMagnitude: number) : Velocity
}

export default IGravityCalcStrategy;